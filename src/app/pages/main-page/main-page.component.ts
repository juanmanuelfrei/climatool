import { Component, OnInit } from '@angular/core';

import { CountriesService } from '../../services/countries.service';
import { WeatherService } from '../../services/weather.service';
import { LoginService } from '../../services/login.service';

import { Weather } from '../../models/weather';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {


  public daysOfTheWeek = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
  public day: string; 
  public week: any = [];

  public countries: string[]; 
  public cities: string[];

  public country: string; 
  public city: string = null; 

  public weatherStatus: Weather[] = [];
  
  constructor(private _countries: CountriesService, private _weather: WeatherService, public loginService: LoginService) {
    this.setNull();
    console.log(this.country);
  }
  
  ngOnInit(): void {
    this.getCountries();
    this.getWeek();
  }

  private setNull(){
    this.cities = [];
    this.weatherStatus = [];
    this.weatherStatus[0] = {description:null, wind:null,humidity:null,celsius:null,farenheit:null };
  }

  private getCountries() {
    return this._countries.getCountries().subscribe( (c:any) => {
      this.countries = c.map(x => x.name);
    });
  }

  private getCities(country: string) {
     return this._countries.getCities(country).subscribe( (c:any) => this.cities = c.data ) 
  }  

  private getWeek(){
    let dayNumber = new Date().getDay();
    this.day = this.daysOfTheWeek[dayNumber];
    dayNumber++;
    for (let i = dayNumber; i <= 6; i++) {
      this.week.push(this.daysOfTheWeek[i]);
      dayNumber++;
    }
    dayNumber = new Date().getDay();
    dayNumber++;
    if (this.week.length < 5) {
      let i = 0
      while (this.week.length <= 4) {
        this.week.push(this.daysOfTheWeek[i]);
        i++;
      }
    }
  }

  onChange(country: string){
    console.log(country);
    this.getCities(country);
    this.setNull();
  }

  search(){
    this._weather.getWeatherByCityName(this.city).subscribe( (weather:any) => {
      console.log(weather);
      const weatherOfTheWeek = this.weatherfilter(weather.list);
      const test = [7,15,23,31,39];
      let indexOfWeatherArray = 1;
      this.pushWeather(0,0, weather);
      for (let i = 0; i < weather.list.length; i++) {
        if (test.includes(i)) {
          this.pushWeather(i, indexOfWeatherArray, weather);
          indexOfWeatherArray++;
        }        
      }
    });
  }
 
  private weatherfilter(weatherOfTheWeek) {
    const filteredWeatherData = weatherOfTheWeek.filter( weatherData => weatherData.dt_txt.includes(`18:00:00`) );
    return filteredWeatherData;
  }

  private pushWeather(index: number, indexOfWeatherArray:number, weather){
    this.weatherStatus[indexOfWeatherArray] = {description:null, wind:null,humidity:null,celsius:null,farenheit:null };
    const tempToInt = Math.round(Number(weather.list[index].main["temp"]));
    const celsius = (tempToInt - 273);
    const farenheit = (tempToInt - 273) * (9/5) + 32;
    this.weatherStatus[indexOfWeatherArray].description = weather.list[index].weather[0].main;
    this.weatherStatus[indexOfWeatherArray].wind        = weather.list[index].wind["speed"];
    this.weatherStatus[indexOfWeatherArray].humidity    = weather.list[index].main["humidity"];
    this.weatherStatus[indexOfWeatherArray].celsius     = celsius;
    this.weatherStatus[indexOfWeatherArray].farenheit   = farenheit;
  }

}
