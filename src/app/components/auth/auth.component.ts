import { Input } from '@angular/core';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginUser: FormGroup;
  @Input() LoginState;
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private _login: LoginService) {
    this.createForm();
  }

  ngOnInit(): void {
    if (localStorage.getItem("user")) {
      this._login.userExist = true;
    }
  }

  get InvalidMail() {
    return this.loginUser.get('email').invalid && this.loginUser.get('email').touched;
  }

  get InvalidPassword() {
    return this.loginUser.get('password').invalid && this.loginUser.get('password').touched;
  }

  createForm() {
    this.loginUser = this.formBuilder.group({
      email    : ['', [Validators.required, Validators.email]],
      password : ['', Validators.required]
    });
  }
  
  login() {
    this._login.login(this.loginUser);
    if (this.LoginState && this.loginUser.valid) {
      this.close.emit(false);
    }
  }

}
