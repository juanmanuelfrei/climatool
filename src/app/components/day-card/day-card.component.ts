import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-day-card',
  templateUrl: './day-card.component.html',
  styleUrls: ['./day-card.component.css']
})
export class DayCardComponent implements OnInit {

  @Input() day: string;
  @Input() celsius: number;
  @Input() farenheit: number;

  constructor() {}

  ngOnInit(): void {
  }

}
