import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userExist: boolean = false;

  constructor() { 
    if (localStorage.getItem("user")) {
      this.userExist = true;
    }
  }

  login(loginUser: FormGroup){
    if (loginUser.invalid) {
      return Object.values(loginUser.controls).forEach(control => {
        if (control instanceof FormGroup) {
          // tslint:disable-next-line:no-shadowed-variable
          Object.values(control.controls).forEach(loginUser => control.markAllAsTouched());
        } else {
          control.markAllAsTouched();
        }
      });
    } else {
      const userEmail = loginUser.get("email").value;
      localStorage.setItem("user",userEmail);
      this.userExist = true;
    }
  }

  logout(){
    localStorage.removeItem('user');
    this.userExist = false;
  }
}
