import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  API_KEY = 'ba3a68d6c3f682989bc36fade411f0da'; //KEY de https://openweathermap.org/api

  constructor(private http: HttpClient) { }

  getWeatherByCityName(cityName: string){
    const url = `http://api.openweathermap.org/data/2.5/forecast?q=${cityName},DE&appid=${this.API_KEY}`;
    return this.http.get(url);
  }
}
