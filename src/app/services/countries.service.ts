import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor(private http: HttpClient) {}
  
  getCountries() {
    const url = `https://restcountries.eu/rest/v2/all`;
    return this.http.get(url);
  }

  getCities(country: string) {
    const url = `https://countriesnow.space/api/v0.1/countries/cities`;
    return this.http.post(url, {country: country })
  }


}
