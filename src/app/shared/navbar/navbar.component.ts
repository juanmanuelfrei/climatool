import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  showLogin: boolean = false;

  constructor(public _login: LoginService) {}

  ngOnInit(): void {
  }

  changeState(){
    this.showLogin ? this.showLogin = false : this.showLogin = true;
  }

  logout(){
    this.changeState();
    this._login.logout();
  }

}
