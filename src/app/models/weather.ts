export interface Weather {
    description: string,
    humidity: number,
    wind: number,
    celsius: number,
    farenheit: number,
}