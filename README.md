# AV

Challange realizado con:
-Angular version 10.0.2.
-Bootstrap 5
-HTML
-CSS

Se utilizaron las API de las siguientes paginas: https://restcountries.eu/ , https://documenter.getpostman.com/view/1134062/T1LJjU52 y https://openweathermap.org/current

## Development server

Una vez clonado el repositorio se debe ejecutar el el comando `npm install` en la consola para instalar los modulos de node y las dependencias que se instalaron para el challange.

Una vez que finalice el punto anterior se debe levantar el servidor de manera local con `ng serve` y navegar a `http://localhost:4200/`.

## A saber

La app no muestra datos en Reporte y los siguientes dias hasta que no se ingresa un pais y ciudad.
Se probo usando como pais seleccionado "Germany" y ciudad "Munich".
